#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <termios.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <pthread.h>

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME        "as_hid"
#define DEF_APP_VERSION     "0.1.4.38"

#define DEF_DEV_FNAME_IRDA  "/dev/ttyS3"
#define DEF_SZ_READ         128
#define DEF_SZ_WRITE        DEF_SZ_READ
#define DEF_PORT_TCP        8803
#define DEF_SZ_MAX_CONN     10
#define DEF_SZ_MAX_CNAME    64

#define DEF_PACKET_TERM_C   '@'
#define DEF_PACKET_TERM_STR "@"

////////////////////////////////////////////////////////////////////////////////

typedef struct _clientsinfo
{
    int       clientfd[DEF_SZ_MAX_CONN];
    int       clientsflag[DEF_SZ_MAX_CONN];
    char*     clientsname[DEF_SZ_MAX_CONN];
}clientsinfo;

////////////////////////////////////////////////////////////////////////////////

static bool     flag_quit  = false;
static int      state_loop = 0;
static int      fdtty = -1;

static char     sock_readbuff[DEF_SZ_READ];
static char     readbuff[DEF_SZ_READ];
static char     writebuff[DEF_SZ_WRITE];
static int      writebuff_sz = 0;
static bool     writebuff_rd = false;

struct termios  tio = {0};
struct termios  prev_tio = {0};

static int      server_fdsock = -1;
static int      client_fdsock = -1;
static int      fdsock;
static int      fd_num;

static int       clients[FD_SETSIZE] = {-1};
static int       clients_max;
static socklen_t client_len;

static clientsinfo clientsInfo;

static int      access_granted_id = -1;

static fd_set   readfds;
static fd_set   allfds;

struct sockaddr_in clientaddr;
struct sockaddr_in serveraddr;

pthread_t       th_socket;
pthread_t       th_serial;

////////////////////////////////////////////////////////////////////////////////

void init_clientsinfo( clientsinfo* cinfo )
{
    if ( cinfo != NULL )
    {
        for( int cnt=0; cnt<DEF_SZ_MAX_CONN; cnt++ )
        {
            cinfo->clientfd[ cnt ] = -1;
            cinfo->clientsflag[ cnt ] = -1;
            cinfo->clientsname[ cnt ] = (char*)malloc( DEF_SZ_MAX_CNAME );
            memset( cinfo->clientsname[ cnt ], 0, DEF_SZ_MAX_CNAME );
#ifdef DEBUG
            strcpy( cinfo->clientsname[ cnt ], "TESTNAME" );
#endif // DEBUG
        }
    }
}

void final_clientsinfo( clientsinfo* cinfo )
{
    if ( cinfo != NULL )
    {
        for( int cnt=0; cnt<DEF_SZ_MAX_CONN; cnt++ )
        {
            cinfo->clientfd[ cnt ] = -1;
            cinfo->clientsflag[ cnt ] = -1;
            free( cinfo->clientsname[ cnt ] );
            cinfo->clientsname[ cnt ] = NULL;
        }
    }
}

void set_clientinfo( clientsinfo* cinfo, int idx, int fd, int flag, const char* name )
{
    if ( cinfo != NULL )
    {
        if ( idx < DEF_SZ_MAX_CONN )
        {
            cinfo->clientfd[ idx ] = fd;
            cinfo->clientsflag[ idx ] = flag;
            if ( name != NULL )
            {
                strcpy( cinfo->clientsname[ idx ], name );
            }
        }
    }
}

void set_name_clientinfo( clientsinfo* cinfo, int idx, const char* name )
{
    if ( cinfo != NULL )
    {
        if ( idx < DEF_SZ_MAX_CONN )
        {
            if ( cinfo->clientfd[ idx ] > 0 )
            {
                if ( name != NULL )
                {
                    memset( cinfo->clientsname[ idx ], 0, DEF_SZ_MAX_CNAME );
                    strncpy( cinfo->clientsname[ idx ], name, DEF_SZ_MAX_CNAME );
                }
            }
        }
    }
}

void clear_clientinfo( clientsinfo* cinfo, int idx )
{
    if ( cinfo != NULL )
    {
        if ( idx < DEF_SZ_MAX_CONN )
        {
            cinfo->clientfd[ idx ] = -1;
            cinfo->clientsflag[ idx ] = -1;
            memset( cinfo->clientsname[ idx ], 0, DEF_SZ_MAX_CNAME );
        }
    }
}

int get_first_idx_clientinfo( clientsinfo* cinfo )
{
    if ( cinfo != NULL )
    {
        for( int cnt=0; cnt<DEF_SZ_MAX_CONN; cnt++ )
        {
            if ( cinfo->clientfd[ cnt ] > 0 )
            {
                return cnt;
            }
        }
    }

    return -1;
}

int get_empty_idx_clientinfo( clientsinfo* cinfo )
{
    if ( cinfo != NULL )
    {
        for( int cnt=0; cnt<DEF_SZ_MAX_CONN; cnt++ )
        {
            if ( cinfo->clientfd[ cnt ] < 0 )
            {
                return cnt;
            }
        }
    }

    return -1;
}

int find_idx_clientsinfo( clientsinfo* cinfo, int fds )
{
    if ( cinfo != NULL )
    {
        for( int cnt=0; cnt<DEF_SZ_MAX_CONN; cnt++ )
        {
            if ( cinfo->clientfd[ cnt ] == fds )
            {
                return cnt;
            }
        }
    }

    return -1;
}

int get_count_connection( clientsinfo* cinfo )
{
    int ncounts = 0;

    if ( cinfo != NULL )
    {
        for( int cnt=0; cnt<DEF_SZ_MAX_CONN; cnt++ )
        {
            if ( cinfo->clientfd[ cnt ] > 0 )
            {
                ncounts++;
            }
        }
    }

    return ncounts;
}

bool open_port()
{
    fdtty = open( DEF_DEV_FNAME_IRDA, O_RDWR | O_NOCTTY );

    if ( fdtty >= 0 )
    {
        tcgetattr( fdtty, &prev_tio );

        tio.c_iflag = IGNPAR;
        tio.c_oflag = 0;
        tio.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
        tio.c_lflag = 0;

        tio.c_cc[VTIME] = 0;
        tio.c_cc[VMIN]  = 1;

        tcflush( fdtty, TCIFLUSH );
        tcsetattr( fdtty, TCSANOW, &tio );

        return true;
    }

    return false;
}

bool close_port()
{
#ifdef DEBUG
    printf("[close tty]");
#endif // DEBUG
    if ( fdtty >= 0 )
    {
        tcsetattr( fdtty, TCSANOW, &prev_tio );

        close( fdtty );
        fdtty = -1;

#ifdef DEBUG
    printf("[done]");
#endif // DEBUG

        return true;
    }

#ifdef DEBUG
    printf("[failure]");
#endif // DEBUG

    return false;
}

bool open_socket()
{
    if ( (server_fdsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP )) < 0 )
    {
        perror("socket error : ");
        return false;
    }

    int optval = 1;
    setsockopt( server_fdsock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval) );

    bzero(&serveraddr, sizeof(serveraddr));

    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl( INADDR_ANY );
    serveraddr.sin_port = htons( DEF_PORT_TCP );

    int state = bind (server_fdsock, (struct sockaddr *)&serveraddr, sizeof(serveraddr));

    if (state == -1)
    {
        close( server_fdsock );
        perror("bind error : ");
        return false;
    }

    state = listen(server_fdsock, 5);
    if (state == -1)
    {
        close( server_fdsock );
        perror("listen error : ");
        return false;
    }

    return true;
}

bool close_socket()
{
#ifdef DEBUG
    printf("[close socket]");
#endif // DEBUG

    for ( int cnt=0; cnt<FD_SETSIZE; cnt++ )
    {
        if (clients[cnt] >= 0)
        {
#ifdef DEBUG
        printf("[%d->%d]", cnt, clients[cnt] );
#endif // DEBUG
            close( clients[cnt] );
            clients[cnt] = -1;
        }
    }

    return true;
}

bool check_cmds( const char* buff, const char* cmds )
{
	if ( ( buff == NULL ) || ( cmds == NULL ) )
		return false;

	int szcmds = strlen( cmds );

	if ( strncmp( buff, cmds, szcmds ) == 0 )
		return true;

	return false;
}

void* do_loop_serial( void* p )
{
    state_loop = 0;

    if ( fdtty >= 0 )
    {
        state_loop = 1;

        while( flag_quit == false )
        {
            memset( readbuff, 0, DEF_SZ_READ );

            int szread = read( fdtty, readbuff, DEF_SZ_READ );
            if ( szread > 0 )
            {
#ifdef DEBUG
#if 0
                printf("## TTY data came(%d bytes):\n", szread);
                printf("%s\n",);
#else
                printf("%s", readbuff );
#endif
#endif /// of DEBUG
                if ( writebuff_rd == false )
                {
                    writebuff_rd = true;

                    memcpy( writebuff, readbuff, szread );
                    writebuff_sz = szread;

                    int szsentcnt = 0;

#ifdef DEBUG
                    printf("## TTY to TCP, %d bytes .. ", writebuff_sz );
#endif /// of DEBUG
                    //write( client_fdsock, writebuff, writebuff_sz );
                    for (int cnt = 0; cnt<FD_SETSIZE; cnt++)
                    {
                        if ( ( clients[cnt] >= 0 ) && ( clients[cnt] != server_fdsock ) )
                        {
                            //write( clients[cnt], writebuff, writebuff_sz );
                            send( clients[cnt], writebuff, writebuff_sz, MSG_NOSIGNAL );
                            szsentcnt++;
                        }
                    }
#ifdef DEBUG
                    if ( szsentcnt > 0 )
                    {
                        printf("Ok.\n");
                    }
                    else
                    {
                        printf("No clients.\n");
                    }
#endif /// of DEBUG
                    memset( writebuff, 0x00, DEF_SZ_WRITE );

                    writebuff_rd = false;
                }
            }
        }
    }

    state_loop = -1;

    return NULL;
}

void set_next_grant()
{
    if ( get_count_connection( &clientsInfo ) > 0 )
    {
        int nidx = get_first_idx_clientinfo( &clientsInfo );
        if ( nidx >= 0 )
        {
            access_granted_id = clientsInfo.clientfd[ nidx ];
        }
    }
}

int process_custom_cmd( int fds, const char* cmd )
{
    if ( check_cmds( cmd, ">>>quit" ) == true )
    {
        //write( fds, "ack-close\n", 10 );
        send( fds, "ack-close\n", 10, MSG_NOSIGNAL );
        close( fds );
        FD_CLR( fds, &readfds );

        int fidx = find_idx_clientsinfo( &clientsInfo, fds );
        if ( fidx >= 0 )
        {
            clear_clientinfo( &clientsInfo, fidx );
        }

        if ( access_granted_id == fds )
        {
            access_granted_id = -1;

            set_next_grant();
        }

        return -1;
    }
    else
    if ( check_cmds( cmd, ">>>getgrantedid" ) == true )
    {
        char tmpstr[32] = {0};

        sprintf( tmpstr, "%d", access_granted_id );
        //write( fdsock, tmpstr, strlen( tmpstr ) );
        send( fdsock, tmpstr, strlen( tmpstr ), MSG_NOSIGNAL );

        return 0;
    }
    else
    if ( check_cmds( cmd, ">>>getmyid" ) == true )
    {
        char tmpstr[32] = {0};

        sprintf( tmpstr, "%d", fdsock );
        //write( fdsock, tmpstr, strlen( tmpstr ) );
        send( fdsock, tmpstr, strlen( tmpstr ), MSG_NOSIGNAL );

        return 0;
    }
    else
    if ( check_cmds( cmd, ">>>getconnectioncount" ) == true )
    {
        char tmpstr[32] = {0};
        int  conns = 0;

        for( int cnt=0; cnt<DEF_SZ_MAX_CONN; cnt++ )
        {
            if ( clientsInfo.clientfd[ cnt ] > 0 )
            {
                conns++;
            }
        }

        sprintf( tmpstr, "%d", conns );
        //write( fdsock, tmpstr, strlen( tmpstr ) );
        send( fdsock, tmpstr, strlen( tmpstr ), MSG_NOSIGNAL );

        return 0;
    }
    else
    if ( check_cmds( cmd, ">>>getconnectionlist" ) == true )
    {
        char tmpstr[128] = {0};

        for( int cnt=0; cnt<DEF_SZ_MAX_CONN; cnt++ )
        {
            if ( strlen( clientsInfo.clientsname[ cnt ] ) > 0 )
            {
                sprintf( tmpstr, "%s;", clientsInfo.clientsname[ cnt ] );
                //write( fdsock, tmpstr, strlen( tmpstr ) );
                send( fdsock, tmpstr, strlen( tmpstr ), MSG_NOSIGNAL );
            }
            else
            {
                sprintf( tmpstr, "%d;", clientsInfo.clientfd[ cnt ] );
                //write( fdsock, tmpstr, strlen( tmpstr ) );
                send( fdsock, tmpstr, strlen( tmpstr ), MSG_NOSIGNAL );
            }
        }

        return 0;
    }
    else
    if ( check_cmds( cmd, ">>>getconnectionidlist" ) == true )
    {
        char tmpstr[128] = {0};

        for( int cnt=0; cnt<DEF_SZ_MAX_CONN; cnt++ )
        {
            sprintf( tmpstr, "%d;", clientsInfo.clientfd[ cnt ] );
            //write( fdsock, tmpstr, strlen( tmpstr ) );
            send( fdsock, tmpstr, strlen( tmpstr ), MSG_NOSIGNAL );
        }

        return 0;
    }
    else
    if ( check_cmds( cmd, ">>>getversion" ) == true )
    {
        //write( fdsock, DEF_APP_VERSION, strlen( DEF_APP_VERSION ) );
        send( fdsock, DEF_APP_VERSION, strlen( DEF_APP_VERSION ), MSG_NOSIGNAL );

        return 0;
    }
    else
    if ( check_cmds( cmd, ">>>setmyname:" ) == true )
    {
        char* fspos = strstr( cmd, ">>>setmyname:" );
        fspos += strlen( ">>>setmyname:" );

        int fidx = find_idx_clientsinfo( &clientsInfo, fds );
        if ( fidx >= 0 )
        {
            set_name_clientinfo( &clientsInfo, fidx, fspos );
        }

        return 0;
    }
    else
    if ( check_cmds( cmd, ">>>getmyname" ) == true )
    {
        int fidx = find_idx_clientsinfo( &clientsInfo, fds );
        if ( fidx >= 0 )
        {
            send( fdsock,
                  clientsInfo.clientsname[ fidx ],
                  strlen( clientsInfo.clientsname[ fidx ] ),
                  MSG_NOSIGNAL );
        }
    }

    return 0;
}

void* do_loop_socket( void* p )
{
    client_fdsock = server_fdsock;

    int maxi = -1;

    int maxfd = server_fdsock;

    for (int cnt = 0; cnt<FD_SETSIZE; cnt++)
    {
        clients[cnt] = -1;
    }

    FD_ZERO(&readfds);
    FD_SET(server_fdsock, &readfds);

    while( flag_quit == false )
    {
        int cnt = 0;
        allfds = readfds;
        fd_num = select(maxfd + 1 , &allfds, (fd_set *)0, (fd_set *)0, NULL);

        if ( FD_ISSET( server_fdsock, &allfds ) )
        {
            // Check maximum connection reached ...
            if ( clients_max > DEF_SZ_MAX_CONN )
                break;

            client_len = sizeof(clientaddr);
            // accept 한다.
            client_fdsock = accept(server_fdsock, (struct sockaddr *)&clientaddr, &client_len);

            for (cnt=0; cnt<FD_SETSIZE; cnt++)
            {
                if ( clients[cnt] < 0 )
                {
                    clients[cnt] = client_fdsock;

                    if ( client_fdsock != server_fdsock )
                    {
                        if ( access_granted_id < 0 )
                        {
                            access_granted_id = client_fdsock;
                        }

                        int leftidx = get_empty_idx_clientinfo( &clientsInfo );

                        if ( leftidx >= 0 )
                        {
                            set_clientinfo( &clientsInfo, leftidx, client_fdsock, 0, NULL );
                        }
                    }
                    break;
                }
            }

            if ( cnt == FD_SETSIZE )
            {
                close( fdsock );
                clients[cnt] = -1;
            }

            FD_SET( client_fdsock, &readfds );

            if ( client_fdsock > maxfd )
            {
                maxfd = client_fdsock;
            }

            if ( cnt > maxi )
            {
                maxi = cnt;
                clients_max = maxi;
            }

            if ( --fd_num <= 0 )
                continue;
        }

        for (cnt=0; cnt<=maxi; cnt++)
        {
            if ( ( fdsock = clients[ cnt ] ) < 0 )
                continue;

            if ( FD_ISSET( fdsock, &allfds ) )
            {
                memset( sock_readbuff, 0, DEF_SZ_READ );

                int szread = read( fdsock, sock_readbuff, DEF_SZ_READ );

                if ( szread <= 0 ) /// Maybe it must be disconnected ....
                {
                    close( fdsock );
                    FD_CLR( fdsock, &readfds );

                    int fidx = find_idx_clientsinfo( &clientsInfo, clients[cnt] );

                    if ( fidx >= 0 )
                    {
                        clear_clientinfo( &clientsInfo, fidx );
                    }

                    if ( clients[cnt] == access_granted_id )
                    {
                        access_granted_id = -1;
                        set_next_grant();
                    }

                    clients[cnt] = -1;
                }
                else
                {
                    if ( check_cmds( sock_readbuff, ">>>" ) == true )
                    {
                        int reti = process_custom_cmd( fdsock, sock_readbuff );
                        if ( reti < 0 )
                        {
                            if ( reti == -1 )
                            {
                                clients[cnt] = -1;
                            }
                            break;
                        }
                    }
                    else
                    if ( ( fdtty >= 0 ) && ( access_granted_id == fdsock ) )
                    {
                        // write socket to tty.
                        char* pStrStart = NULL;

                        pStrStart = strstr( sock_readbuff, "j" );
                        if ( pStrStart == NULL )
                        {
                            pStrStart = strstr( sock_readbuff, "k" );

                            if ( pStrStart == NULL )
                            {
                                pStrStart = strstr( sock_readbuff, "m" );
                            }
                        }

                        if( pStrStart != NULL )
                        {
                            static char tmpsendstr[DEF_SZ_READ];

                            memset( tmpsendstr, 0, DEF_SZ_READ );
#ifdef DEBUG
                            printf("## TCP to TTY : " );
#endif /// of DEBUG

                            if ( strstr( pStrStart, DEF_PACKET_TERM_STR ) == NULL )
                            {
                                sprintf( tmpsendstr, "%s %c", pStrStart, DEF_PACKET_TERM_C );
                            }
                            else
                            {
                                sprintf( tmpsendstr, "%s", pStrStart );
                            }

                            int sztmpstr = strlen( tmpsendstr );
#ifdef DEBUG
                            printf("%s (%d bytes) : ", tmpsendstr, sztmpstr );
#endif /// of DEBUG
                            write( fdtty, tmpsendstr, sztmpstr );
#ifdef DEBUG
                            printf("Ok.\n");
#endif /// of DEBUG
                        }
#ifdef DEBUG
                        else
                        {
                            printf( "\n#income: %s\n", sock_readbuff );
                        }
#endif /// of DEBUG
                    }
                }

                if ( --fd_num <= 0 )
                    break;
            }
        }
    }

    return NULL;
}

bool sigint_mutexed = false;

void sigint_handler( int signo)
{
    if ( sigint_mutexed == false )
    {
        sigint_mutexed = true;
#ifdef DEBUG
        printf("[%s] signal interrupted !\n", DEF_APP_NAME);
        printf("## Signal interrupted.\n");
        //signal( SIGINT, NULL );
        printf("## Terminating threads ... ");
#endif /// of DEBUG

        flag_quit = true;
        usleep( 10000 );

#ifdef DEBUG
        printf("(1)");
#endif /// of DEBUG

        if ( th_socket > 0  )
        {
            pthread_kill( th_socket, SIGINT );
            //pthread_join( th_socket, NULL );
        }

#ifdef DEBUG
        printf("(2)");
#endif /// of DEBUG
        if ( th_serial > 0 )
        {
            pthread_kill( th_serial, SIGINT );
            //pthread_join( th_serial, NULL );
        }

        close_port();
        close_socket();
#ifdef DEBUG
        printf("(done)\n");
        printf("\n#Signal interruption done.##\n");
#endif /// of DEBUG

#ifndef DEBUG
        printf("Signal interruption : service down , %s \n", DEF_APP_NAME);
#endif // DEBUG

        final_clientsinfo( &clientsInfo );

        exit( 0 );

    }
}

int main( int argc, char** argv )
{
    printf( "%s: Pass-thru HID2TCP daemon for AnyStream. version %s\n",
            DEF_APP_NAME,
            DEF_APP_VERSION );
    printf( "(C)Copyright 2015~2016, 3IWare - rage.kim@3iware.com\n" );
#ifdef DEBUG
    printf( "* DEBUG build : user message enabled *\n");
    printf( "\n" );
#endif // DEBUG

    signal( SIGINT, sigint_handler );

    sleep(1);

#ifdef DEBUG
    printf("# Open: serial ... ");
#endif // DEBUG
    if ( open_port() == false )
    {
#ifdef DEBUG
        printf( ":Error: serial not be open.\n" );
#endif // DEBUG
        return 0;
    }
#ifdef DEBUG
    printf("Ok.\n");
#endif // DEBUG

#ifdef DEBUG
    printf("# Open: TCP server ... ");
#endif // DEBUG
    if ( open_socket() == false )
    {
#ifdef DEBUG
        printf( ":Error: TCP port not be open.\n" );
#endif // DEBUG
        return 0;
    }
#ifdef DEBUG
    printf("Ok.\n");
#endif // DEBUG

    int reti = -1;

#ifdef DEBUG
    printf("# Initializing clients info ... ");
#endif // DEBUG
    init_clientsinfo( &clientsInfo );
#ifdef DEBUG
    printf("Ok.\n");
#endif // DEBUG

#ifdef DEBUG
    printf("-- Creating serial thread ... ");
#endif // DEBUG
    reti = pthread_create( &th_serial, NULL, do_loop_serial, NULL );
#ifdef DEBUG
    if ( reti >= 0 )
    {
#ifdef DEBUG
        printf("Ok.\n");
#endif // DEBUG
    }
    else
    {
#ifdef DEBUG
        printf("Failure : %d\n", reti );
#endif // DEBUG
        return reti;
    }
#endif // DEBUG

#ifdef DEBUG
    printf("-- Creating socket thread ... ");
#endif // DEBUG
    reti = pthread_create( &th_socket, NULL, do_loop_socket, NULL );
#ifdef DEBUG
    if ( reti >= 0 )
    {
#ifdef DEBUG
        printf("Ok.\n");
#endif // DEBUG
    }
    else
    {
#ifdef DEBUG
        printf("Failure : %d\n", reti );
#endif // DEBUG
        return reti;
    }
#endif // DEBUG

    pthread_join( th_socket, NULL );
    pthread_join( th_serial, NULL );

    final_clientsinfo( &clientsInfo );

#ifdef DEBUG
    printf("-- Finishing work ... ");
#endif // DEBUG

    close_port();
    close_socket();

    printf("\n");
    printf("[%s] Terminated.\n", DEF_APP_NAME);
    printf("\n");

    return 0;
}
